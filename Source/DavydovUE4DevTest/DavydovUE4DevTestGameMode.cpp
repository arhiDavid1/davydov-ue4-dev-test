// Copyright Epic Games, Inc. All Rights Reserved.

#include "DavydovUE4DevTestGameMode.h"
#include "DavydovUE4DevTestHUD.h"
#include "DavydovUE4DevTestCharacter.h"
#include "UObject/ConstructorHelpers.h"

ADavydovUE4DevTestGameMode::ADavydovUE4DevTestGameMode()
	: Super()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/FirstPersonCPP/Blueprints/FirstPersonCharacter"));
	DefaultPawnClass = PlayerPawnClassFinder.Class;

	// use our custom HUD class
	HUDClass = ADavydovUE4DevTestHUD::StaticClass();
}
