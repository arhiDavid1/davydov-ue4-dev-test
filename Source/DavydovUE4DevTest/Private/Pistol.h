// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "EquipableItemBase.h"
#include "Pistol.generated.h"

/**
 * 
 */
UCLASS()
class APistol : public AEquipableItemBase
{
	GENERATED_BODY()

	APistol();

	virtual void ItemUnequiped(bool DestroyOnUnequip = false, bool ShouldSimulatePhysicsOnUnequip = true) override;

public:
	virtual void UseThisItem() override;

	virtual void ThisItemHasBeenEquipped(UHandItemManager* ManagerForThisItem) override;

	/** Gun mesh: 1st person view (seen only by self) */
	UPROPERTY(VisibleDefaultsOnly, Category = Mesh)
	USkeletalMeshComponent* FP_Gun;

	/** Projectile class to spawn */
	UPROPERTY(EditDefaultsOnly, Category = Projectile)
	TSubclassOf<class ADavydovUE4DevTestProjectile> ProjectileClass;

	/** Location on gun mesh where projectiles should spawn. */
	UPROPERTY(EditDefaultsOnly, Category = Mesh)
	USceneComponent* FP_MuzzleLocation;

	/** Gun muzzle's offset from the characters location */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Gameplay)
	FVector GunOffset;

private:
	void FirePistol();
};
