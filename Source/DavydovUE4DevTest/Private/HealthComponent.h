// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "HealthComponent.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE( FCharacterDiedDelegate );
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam( FCharacterTookDamage, float, NewCurrentHp);

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class UHealthComponent : public UActorComponent
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Health parameters", meta = (AllowPrivateAccess = "true"))
	float DefaultHealth;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Health parameters", meta = (AllowPrivateAccess = "true"))
	float MaximumHealth;

public:	
	// Sets default values for this component's properties
	UHealthComponent();

	void ChangeHealthAmount(float ChangeAmount);

	UPROPERTY(BlueprintAssignable, Category = "Events")
	FCharacterDiedDelegate OnCharacterDied;

	UPROPERTY(BlueprintAssignable, Category = "Events")
	FCharacterTookDamage OnCharacterTookDamage;

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

private:
	float CurrentHealth;

	void CharacterDied();

	bool bHasDied;
};
