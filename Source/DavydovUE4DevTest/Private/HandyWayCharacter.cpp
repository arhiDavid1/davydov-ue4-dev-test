// Fill out your copyright notice in the Description page of Project Settings.


#include "HandyWayCharacter.h"

// Custom classes
#include "NoVRHandItemManager.h"
#include "HealthComponent.h"
#include "HandyWayGameMode.h"

//UE engine classes
#include "Components/WidgetInteractionComponent.h"
#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/SphereComponent.h"
#include "HeadMountedDisplayFunctionLibrary.h"
#include "MotionControllerComponent.h"
#include "XRMotionControllerBase.h" // for FXRMotionControllerBase::RightHandSourceId

// Sets default values
AHandyWayCharacter::AHandyWayCharacter()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	RootComponent = GetCapsuleComponent();

	// Create a CameraComponent	
	FirstPersonCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("FirstPersonCamera"));
	FirstPersonCameraComponent->SetupAttachment(GetCapsuleComponent());
	FirstPersonCameraComponent->SetRelativeLocation(FVector(-39.56f, 1.75f, 64.f)); // Position the camera
	FirstPersonCameraComponent->bUsePawnControlRotation = true;

	R_HandCollisionSphere = CreateDefaultSubobject<USphereComponent>(TEXT("R_HandCollisionSphere"));
	L_HandCollisionSphere = CreateDefaultSubobject<USphereComponent>(TEXT("L_HandCollisionSphere"));

	// Uncomment the following line to turn motion controllers on:
	//bUsingMotionControllers = true;

	if (bUsingMotionControllers)
	{
		R_HandItemManager = CreateDefaultSubobject<UHandItemManager>(TEXT("R_HandItemmanager"));
		L_HandItemManager = CreateDefaultSubobject<UHandItemManager>(TEXT("L_HandItemmanager"));
	}
	else
	{
		R_HandItemManager = CreateDefaultSubobject<UNoVRHandItemManager>(TEXT("R_HandItemmanager"));
		L_HandItemManager = CreateDefaultSubobject<UNoVRHandItemManager>(TEXT("L_HandItemmanager"));
	}

	R_HandItemManager->ComponentTags.Add(FName("R_HandItemManager"));
	L_HandItemManager->ComponentTags.Add(FName("L_HandItemManager"));

	if (bUsingMotionControllers)
	{
		// Create VR Controllers.
		R_MotionController = CreateDefaultSubobject<UMotionControllerComponent>(TEXT("R_MotionController"));
		R_MotionController->MotionSource = FXRMotionControllerBase::RightHandSourceId;
		R_MotionController->SetupAttachment(RootComponent);
		R_HandCollisionSphere->SetupAttachment(R_MotionController);
		
		L_MotionController = CreateDefaultSubobject<UMotionControllerComponent>(TEXT("L_MotionController"));
		L_MotionController->SetupAttachment(RootComponent);
		L_HandCollisionSphere->SetupAttachment(L_MotionController);
	}
}

void AHandyWayCharacter::PostInitializeComponents()
{
	Super::PostInitializeComponents();
	if (!bUsingMotionControllers)
	{
		// Attach hand collision spheres to Scene components that have tags "LeftNoVRAttachmentPoint" and "RightNoVRAttachmentPoint" respectively
		//TODO: make it so collision sphere attachment happens on blueprint child class rebuild - so changes in position are visible during in engine work
		//TODO: Write out error if there are more then one components with the same tag
		auto ComponentsWithLeftTag = GetComponentsByTag(USceneComponent::StaticClass(), TEXT("LeftNoVRAttachmentPoint"));
		if (ComponentsWithLeftTag.Num() > 0)
		{
			USceneComponent* LeftAttachmentPoint = Cast<USceneComponent>(ComponentsWithLeftTag.Last());
			L_HandCollisionSphere->AttachToComponent(LeftAttachmentPoint, FAttachmentTransformRules::SnapToTargetNotIncludingScale);
		}

		auto ComponentsWithRightTag = GetComponentsByTag(USceneComponent::StaticClass(), TEXT("RightNoVRAttachmentPoint"));
		if (ComponentsWithRightTag.Num() > 0)
		{
			USceneComponent* RightAttachmentPoint = Cast<USceneComponent>(ComponentsWithRightTag.Last());
			R_HandCollisionSphere->AttachToComponent(RightAttachmentPoint, FAttachmentTransformRules::SnapToTargetNotIncludingScale);
		}
	}

	R_HandItemManager->SetHandCollisionSphere(R_HandCollisionSphere);
	L_HandItemManager->SetHandCollisionSphere(L_HandCollisionSphere);

	auto L_HealthComponent = Cast<UHealthComponent>(GetComponentByClass(UHealthComponent::StaticClass()));
	if (L_HealthComponent)
	{
		HealthComponent = L_HealthComponent;
		HealthComponent->OnCharacterDied.AddDynamic(this, &AHandyWayCharacter::StartDeathSequence);
	}
}

// Called when the game starts or when spawned
void AHandyWayCharacter::BeginPlay()
{
	Super::BeginPlay();

	auto Components = GetComponentsByTag(UWidgetInteractionComponent::StaticClass(),FName("LeftInteractionWidget"));
	for (UActorComponent* Component : Components)
	{
		UWidgetInteractionComponent* Local_WidgetInteractor = Cast<UWidgetInteractionComponent>(Component);
		if (Local_WidgetInteractor)
		{
			L_WidgetInteractor = Local_WidgetInteractor;
		}
	}

	Components = GetComponentsByTag(UWidgetInteractionComponent::StaticClass(), FName("RightInteractionWidget"));
	for (UActorComponent* Component : Components)
	{
		UWidgetInteractionComponent* Local_WidgetInteractor = Cast<UWidgetInteractionComponent>(Component);
		if (Local_WidgetInteractor)
		{
			R_WidgetInteractor = Local_WidgetInteractor;
		}
	}
}

//////////////////////////////////////////////////////////////////////////
// Input

// Called to bind functionality to input
void AHandyWayCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	// set up gameplay key bindings
	check(PlayerInputComponent);

	PlayerInputComponent->BindAction("ResetVR", IE_Pressed, this, &AHandyWayCharacter::OnResetVR);

	PlayerInputComponent->BindAction("UseItemInLeftHand", IE_Pressed, this, &AHandyWayCharacter::UseItemInLeftHand);
	PlayerInputComponent->BindAction("UseItemInLeftHand", IE_Released, this, &AHandyWayCharacter::StopUsingItemInLeftHand);
	PlayerInputComponent->BindAction("DropItemInLeftHand", IE_Pressed, this, &AHandyWayCharacter::DropItemInLeftHand);
	PlayerInputComponent->BindAction("UseItemInRightHand", IE_Pressed, this, &AHandyWayCharacter::UseItemInRightHand);
	PlayerInputComponent->BindAction("UseItemInRightHand", IE_Released, this, &AHandyWayCharacter::StopUsingItemInRightHand);
	PlayerInputComponent->BindAction("DropItemInRightHand", IE_Pressed, this, &AHandyWayCharacter::DropItemInRightHand);
	PlayerInputComponent->BindAction("OpenMenu", IE_Pressed, this, &AHandyWayCharacter::ToggleMenu);

	// Bind movement events
	PlayerInputComponent->BindAxis("MoveForward", this, &AHandyWayCharacter::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &AHandyWayCharacter::MoveRight);

	PlayerInputComponent->BindAxis("Turn", this, &APawn::AddControllerYawInput);
	PlayerInputComponent->BindAxis("LookUp", this, &APawn::AddControllerPitchInput);
}

// Called every frame
void AHandyWayCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

float AHandyWayCharacter::TakeDamage(float DamageAmount, FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser)
{
	float ActualDamage = DamageAmount;
	HealthComponent->ChangeHealthAmount(-DamageAmount);
	return ActualDamage;
}

void AHandyWayCharacter::MoveForward(float Value)
{
	if (Value != 0.0f)
	{
		// add movement in that direction
		AddMovementInput(GetActorForwardVector(), Value);
	}
}

void AHandyWayCharacter::MoveRight(float Value)
{
	if (Value != 0.0f)
	{
		// add movement in that direction
		AddMovementInput(GetActorRightVector(), Value);
	}
}

void AHandyWayCharacter::UseItemInRightHand()
{
	R_HandItemManager->UseEquippedItem();
	if (R_WidgetInteractor->IsActive())
	{
		R_WidgetInteractor->PressPointerKey(EKeys::LeftMouseButton);
	}
}

void AHandyWayCharacter::StopUsingItemInRightHand()
{
	R_HandItemManager->StopUsingItem();
	if (R_WidgetInteractor->IsActive())
	{
		R_WidgetInteractor->ReleasePointerKey(EKeys::LeftMouseButton);
	}
}

void AHandyWayCharacter::DropItemInRightHand()
{
	R_HandItemManager->ReleaseHeldItem();
}

void AHandyWayCharacter::UseItemInLeftHand()
{
	L_HandItemManager->UseEquippedItem();
	if (L_WidgetInteractor->IsActive())
	{
		L_WidgetInteractor->PressPointerKey(EKeys::LeftMouseButton);
	}
}

void AHandyWayCharacter::StopUsingItemInLeftHand()
{
	L_HandItemManager->StopUsingItem();
	if (L_WidgetInteractor->IsActive())
	{
		L_WidgetInteractor->ReleasePointerKey(EKeys::LeftMouseButton);
	}
}

void AHandyWayCharacter::DropItemInLeftHand()
{
	L_HandItemManager->ReleaseHeldItem();
}

void AHandyWayCharacter::ToggleMenu()
{
	if (!MenuWidget) return;

	L_WidgetInteractor->ToggleActive();
	R_WidgetInteractor->ToggleActive();

	if (MenuActor && !MenuActor->IsPendingKill())
	{
		MenuActor->Destroy();
		return;
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("Spawning menu actor"));

		MenuActor = GetWorld()->SpawnActor<AActor>(MenuWidget);
		FVector MenuLocation = GetActorLocation() + GetActorForwardVector() * 500;
		FRotator MenuRotation = GetActorRotation().Add(0,180,0);
		MenuActor->SetActorLocation(MenuLocation);
		MenuActor->SetActorRotation(MenuRotation);
	}
}

void AHandyWayCharacter::StartDeathSequence()
{
	SetCanBeDamaged(false);
	GetController()->UnPossess();
	//TODO: Create death sequence - some time and information must happen between death and respawn
	this->Destroy();
}

void AHandyWayCharacter::OnResetVR()
{
	UHeadMountedDisplayFunctionLibrary::ResetOrientationAndPosition();
}