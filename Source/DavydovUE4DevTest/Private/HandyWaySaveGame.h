// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/SaveGame.h"
#include "HandyWaySaveGame.generated.h"

class AEquipableItemBase;

/**
 * 
 */
UCLASS()
class UHandyWaySaveGame : public USaveGame
{
	GENERATED_BODY()

public:
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	TSubclassOf<AEquipableItemBase> SavedItemInTheRightHand;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	TSubclassOf<AEquipableItemBase> SavedItemInTheLeftHand;
};
