// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "EquipableItemBase.generated.h"

//All items that should be possible to equip in a hand must inherit from this class

class UHandItemManager;

UCLASS(Abstract)
class AEquipableItemBase : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AEquipableItemBase();

	virtual void ItemUnequiped(bool DestroyOnUnequip = false, bool ShouldSimulatePhysicsOnUnequip = true);

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY()
	UHandItemManager* ThisItemManager;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	virtual void UseThisItem();

	virtual void StopUsingThisItem();

	virtual void ThisItemHasBeenEquipped(UHandItemManager* ManagerForThisItem);
};
