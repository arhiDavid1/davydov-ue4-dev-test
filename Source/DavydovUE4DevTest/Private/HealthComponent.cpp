// Fill out your copyright notice in the Description page of Project Settings.


#include "HealthComponent.h"

// Sets default values for this component's properties
UHealthComponent::UHealthComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	MaximumHealth = 100;
	DefaultHealth = MaximumHealth;
	CurrentHealth = DefaultHealth;
}

void UHealthComponent::ChangeHealthAmount(float ChangeAmount)
{
	CurrentHealth += ChangeAmount;
	if (CurrentHealth <= 0)
	{
		CharacterDied();
	}
	OnCharacterTookDamage.Broadcast(CurrentHealth);
}


// Called when the game starts
void UHealthComponent::BeginPlay()
{
	Super::BeginPlay();

	CurrentHealth = DefaultHealth;
	bHasDied = CurrentHealth <= 0;	
}


// Called every frame
void UHealthComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

void UHealthComponent::CharacterDied()
{
	OnCharacterDied.Broadcast();
	bHasDied = true;
}

