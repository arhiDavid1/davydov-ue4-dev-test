// Fill out your copyright notice in the Description page of Project Settings.


#include "BasicGrenade.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "Components/SphereComponent.h"
#include "DrawDebugHelpers.h"

ABasicGrenade::ABasicGrenade()
{
	// Use a sphere as a simple collision representation
	CollisionComp = CreateDefaultSubobject<USphereComponent>(TEXT("SphereComp"));
	CollisionComp->InitSphereRadius(5.0f);
	CollisionComp->BodyInstance.SetCollisionProfileName("Projectile");
	CollisionComp->OnComponentHit.AddDynamic(this, &ABasicGrenade::OnHit);		// set up a notification for when this component hits something blocking

	// Players can't walk on it
	CollisionComp->SetWalkableSlopeOverride(FWalkableSlopeOverride(WalkableSlope_Unwalkable, 0.f));
	CollisionComp->CanCharacterStepUpOn = ECB_No;

	// Set as root component
	RootComponent = CollisionComp;

	//// Use a ProjectileMovementComponent to govern this grenade's movement
	//GrenadeMovement = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("ProjectileComp"));
	//GrenadeMovement->UpdatedComponent = CollisionComp;
	//GrenadeMovement->bRotationFollowsVelocity = true;
	//GrenadeMovement->bShouldBounce = true;
}

void ABasicGrenade::OnHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	if (OtherActor->CanBeDamaged())
	{
		AController* ThisGranadeInstigator = nullptr;
		if (GetInstigator() && GetInstigator()->GetController())
		{
			ThisGranadeInstigator = GetInstigator()->GetController();
		}
		FDamageEvent DamageDetails;
		OtherActor->TakeDamage(GrenadeBluntDamage, DamageDetails, ThisGranadeInstigator, this);
	}

}

void ABasicGrenade::ItemUnequiped(bool DestroyOnUnequip, bool ShouldSimulatePhysicsOnUnequip)
{
	Super::ItemUnequiped(false, ShouldSimulatePhysicsOnUnequip); //Making sure grenade will not be destroyed on unequip
	if (CurrentGranadeState == GrenadeStates::PIN_REMOVED)
		LightFuse();
}

void ABasicGrenade::UseThisItem()
{
	Super::UseThisItem();
	switch (CurrentGranadeState)
	{
	case GrenadeStates::DORMANT:
		PrimeGrenade();
		break;
	case GrenadeStates::PIN_REMOVED:
		LightFuse();
		break;
	case GrenadeStates::LEVER_DROPPED:
		UE_LOG(LogTemp, Warning, TEXT("You are trying to use the grenade that is going to explode before you finish reading this sentence"));
		if (GEngine)
			GEngine->AddOnScreenDebugMessage(-1, 15.f, FColor::Orange, FString::Printf(TEXT("You are trying to use the grenade that is going to explode before you finish reading this sentence")/*, *this->GetName()*/));
		break;
	default:
		UE_LOG(LogTemp, Warning, TEXT("UseThisItem called on the grenade % s.While it is neither dormant, nor pin removed - how did this happen ? If this is expected behavior - review grenade logic and amend this error text"), *this->GetName());
		if (GEngine)
			GEngine->AddOnScreenDebugMessage(-1, 15.f, FColor::Orange, FString::Printf(TEXT("UseThisItem called on the grenade %s. While it is neither dormant, nor pin removed - how did this happen? If this is expected behavior - review grenade logic and amend this error text"), *this->GetName()));
		break;
	}
}

void ABasicGrenade::ThisItemHasBeenEquipped(UHandItemManager* ManagerForThisItem)
{
	Super::ThisItemHasBeenEquipped(ManagerForThisItem);
	StartRecordingMovement();
}

void ABasicGrenade::PrimeGrenade()
{
	CurrentGranadeState = GrenadeStates::PIN_REMOVED;
	if (GEngine)
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Orange, FString::Printf(TEXT("Grenade primed")/*, *this->GetName()*/));
	//TODO: add green or red light to indicate that grenade is primed
}

void ABasicGrenade::LightFuse()
{
	// Start the timer with a specified delay
	if (!FuseTimerHandle.IsValid())
	{
		FuseTimerHandle = FTimerHandle::FTimerHandle();
	}
	if (!GetWorld()) return;
	GetWorld()->GetTimerManager().SetTimer(FuseTimerHandle, this, &ABasicGrenade::ExplodeGrenade, FuseTimer, false);
	CurrentGranadeState = GrenadeStates::LEVER_DROPPED;
	UE_LOG(LogTemp, Warning, TEXT("Fuse lit"));
	if (GEngine)
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Orange, FString::Printf(TEXT("Fuse lit")/*, *this->GetName()*/));
	//TODO: make the light blink to indicate that fuse is running out - the closer the timer to running out - the faster the blinking (WTF is wrong with this sentence?).
}

void ABasicGrenade::ExplodeGrenade()
{
	CurrentGranadeState = GrenadeStates::EXPLODED;
	UE_LOG(LogTemp, Warning, TEXT("BOOM!!!"));
	if (GEngine)
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Orange, FString::Printf(TEXT("BOOM!!!")/*, *this->GetName()*/));

	// Define the sphere parameters
	FVector SphereCenter = GetActorLocation();
	float SphereRadius = GrenadeExplosionRadius;

	// Get the current world
	UWorld* World = GetWorld();
	if (!World) return;

	// Array to store the overlapped actors
	TArray<FOverlapResult> OverlappingObjects;

	// Perform the overlap query
	World->OverlapMultiByObjectType(
		OverlappingObjects,
		SphereCenter,
		FQuat::Identity,
		FCollisionObjectQueryParams::AllObjects,
		FCollisionShape::MakeSphere(SphereRadius)
	);

	DrawDebugSphere(World,SphereCenter,SphereRadius, 16, FColor::Red, false, 5);

	for (const FOverlapResult& Overlap : OverlappingObjects)
	{
		AActor* OverlappingActor = Overlap.GetActor();
		if (OverlappingActor && OverlappingActor != this)
		{
			FDamageEvent DamageDetails;
			AController* ThisGranadeInstigator = GetWorld()->GetFirstPlayerController(); //TODO: Figure out what to do if Instigator is null pointer
			if (GetInstigator() && GetInstigator()->GetController())
			{
				ThisGranadeInstigator = GetInstigator()->GetController();
			}
			OverlappingActor->TakeDamage(GrenadeExplosiveDamage, DamageDetails, ThisGranadeInstigator, this);
		}
	}
	Destroy();
}

void ABasicGrenade::StartRecordingMovement()
{
	//TODO record movement to apply to calculate grenade velocity once it is released
}
