// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "EquipableItemBase.h"
#include "GameFramework/Actor.h"
#include "BasicGrenade.generated.h"

class USphereComponent;
class UProjectileMovementComponent;

UENUM(BlueprintType)
enum class GrenadeStates : uint8
{
	DORMANT UMETA(DisplayName = "Dormant"),
	PIN_REMOVED UMETA(DisplayName = "Pin removed"),
	LEVER_DROPPED UMETA(DisplayName = "Lever dropped"),
	EXPLODED UMETA(DisplayName = "Exploded")
};

/**
 * 
 */
UCLASS()
class ABasicGrenade : public AEquipableItemBase
{
	GENERATED_BODY()
	
	/** Sphere collision component */
	UPROPERTY(VisibleDefaultsOnly, Category = Projectile)
	USphereComponent* CollisionComp;

	/** Grenade movement component */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Movement, meta = (AllowPrivateAccess = "true"))
	UProjectileMovementComponent* GrenadeMovement;

	/** Grenade blunt damage - how much damage will it do by just bonking someone - not exploding */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"))
	float GrenadeBluntDamage = 1;

	/** Grenade explosive damage - how much damage will it do if exploded */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"))
	float GrenadeExplosiveDamage = 200;

	/** Grenade explosion radius - radius of the sphere centered on the grenade where damage will be applied in UU(cm)*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"))
	float GrenadeExplosionRadius = 150;

	/** Grenade fuse timer - how many seconds must pass between grenade switching from state LEVER_DROPPED to EXPLODED */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"))
	float FuseTimer = 5;

public:
	ABasicGrenade();

	/** called when grenade hits something, can be overridden - for example if we want to make impact grenades */
	UFUNCTION()
	virtual void OnHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit);

	/** Returns CollisionComp subobject **/
	USphereComponent* GetCollisionComp() const { return CollisionComp; }
	/** Returns GrenadeMovement subobject **/
	UProjectileMovementComponent* GetProjectileMovement() const { return GrenadeMovement; }

	////////////////////////////
	// Base class overrides
	virtual void ItemUnequiped(bool DestroyOnUnequip = false, bool ShouldSimulatePhysicsOnUnequip = true) override;

	virtual void UseThisItem() override;

	virtual void ThisItemHasBeenEquipped(UHandItemManager* ManagerForThisItem) override;

private:
	//Do not manual switch grenade states - call appropriate functions: PrimeGrenade(), LightFuse() or ExplodeGrenade()
	UPROPERTY(VisibleAnywhere)
	GrenadeStates CurrentGranadeState = GrenadeStates::DORMANT;

	UPROPERTY()
	FTimerHandle FuseTimerHandle;

	virtual void PrimeGrenade();

	//TODO: Add function to UnprimeGrenade the grenade - in case we insert safety pin back.
	//virtual void UnprimeGrenade();

	virtual void LightFuse();

	UFUNCTION()
	virtual void ExplodeGrenade();

	//Start recording actor movement to calculate throw trajectory 
	void StartRecordingMovement();
};
