// Fill out your copyright notice in the Description page of Project Settings.


#include "NoVRHandItemManager.h"
#include "Camera/CameraComponent.h"
#include "Components/SphereComponent.h"
#include "Components/TimelineComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"
#include "DrawDebugHelpers.h"

UNoVRHandItemManager::UNoVRHandItemManager()
{
	ReachedDestination = false;
	IsReadyToMoveAgain = true;
	IsPerformingGesture = false;
}

void UNoVRHandItemManager::BeginPlay()
{
	Super::BeginPlay();

	if (MovementCurve)
	{
		FOnTimelineFloat TimelineCallback;
		FOnTimelineEventStatic TimelineFinishedCallback;

		TimelineCallback.BindUFunction(this, FName("ControlHandMovement"));
		TimelineFinishedCallback.BindUFunction(this, FName("HandReachedDestination"));

		MoveTimeline = NewObject<UTimelineComponent>(this, FName("HandMovementAnimation"));
		MoveTimeline->AddInterpFloat(MovementCurve, TimelineCallback);
		MoveTimeline->SetTimelineFinishedFunc(TimelineFinishedCallback);
		MoveTimeline->RegisterComponent();
	}
}

void UNoVRHandItemManager::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	if (MoveTimeline)
	{
		MoveTimeline->TickComponent(DeltaTime, TickType, ThisTickFunction);
	}
}

void UNoVRHandItemManager::UseEquippedItem()
{
	//Super::UseEquippedItem(); - do not call Super on this function. Instead use ParentClassUseEquippedItem() once it is actually supposed to be used if it was in VR
	//TODO: develop more robust way of switching behaviors depending on item tags
	if (EquippedItem->Tags.Contains(TEXT("EmptyHands")))
	{
		if ((!IsPerformingGesture) && IsReadyToMoveAgain)
		{
			ManagedHandCollisionSphere->DetachFromComponent(FDetachmentTransformRules::KeepWorldTransform);
			IsReadyToMoveAgain = false;
			IsPerformingGesture = true;
			ReachGoal = GetReachDestination();
			MoveHandToDestination();
		}
		return;
	}

	//Default behavior if no special tags were detected
	ParentClassUseEquippedItem();
}

void UNoVRHandItemManager::ParentClassUseEquippedItem()
{
	UHandItemManager::UseEquippedItem();
}

void UNoVRHandItemManager::StopUsingItem()
{
	//Super::StopUsingItem(); - do not call Super on this function. Instead use ParentClassStopUsingItem() once it is actually supposed to be used if it was in VR
}

void UNoVRHandItemManager::ParentClassStopUsingItem()
{
	UHandItemManager::StopUsingItem();
}

void UNoVRHandItemManager::MoveHandToDestination()
{
	MoveTimeline->PlayFromStart();
}

FVector UNoVRHandItemManager::GetReachDestination()
{
	FVector PointOfInterest = GetPointOfInterest();
	FVector StartLocation = ItemAnchorComponent->GetComponentLocation();

	FHitResult HitResult;

	bool bHit = UKismetSystemLibrary::LineTraceSingle(this, StartLocation, PointOfInterest, UEngineTypes::ConvertToTraceType(ECC_WorldDynamic), false, TArray<AActor*>(), EDrawDebugTrace::None, HitResult, true, FLinearColor::Red, FLinearColor::Green, 5.f);

	if (HitResult.bBlockingHit && (HitResult.Distance <= ReachDistance))
	{
		PointOfInterest = HitResult.Location;
	}
	else
	{
		PointOfInterest = StartLocation + ((PointOfInterest - StartLocation).GetSafeNormal() * ReachDistance);
	}
	return PointOfInterest;
}

FVector UNoVRHandItemManager::GetPointOfInterest()
{
	FVector L_ReachGoal = FVector::ZeroVector;
	APlayerController* PlayerController = GetWorld()->GetFirstPlayerController();
	if (PlayerController)
	{
		int32 ViewportSizeX, ViewportSizeY;
		PlayerController->GetViewportSize(ViewportSizeX, ViewportSizeY);  // Get the viewport size

		FVector2D ScreenPosition(ViewportSizeX / 2.f, ViewportSizeY / 2.f);  // Center screen position

		FVector WorldLocation, WorldDirection;
		if (UGameplayStatics::DeprojectScreenToWorld(PlayerController, ScreenPosition, WorldLocation, WorldDirection))
		{
			// WorldLocation contains the world position where the camera is looking at
			// WorldDirection contains the forward vector of the camera
			// ...

			L_ReachGoal = WorldLocation + (WorldDirection * 10000);
			return L_ReachGoal;
		}
	}
	return L_ReachGoal;
}

void UNoVRHandItemManager::ControlHandMovement()
{
	TimelineValue = MoveTimeline->GetPlaybackPosition();
	CurveFloatValue = MovementCurve->GetFloatValue(TimelineValue);

	//TODO: This movement does not look natural, but at least it's reliable - good enough for now.
	FVector LocationDelta = (ReachGoal - ItemAnchorComponent->GetComponentLocation()).GetSafeNormal() * CurveFloatValue;

	if (!IsPerformingGesture)
	{
		ManagedHandCollisionSphere->AddWorldOffset(-LocationDelta);
	}
	else
	{
		ManagedHandCollisionSphere->AddWorldOffset(LocationDelta);
	}
}

void UNoVRHandItemManager::HandReachedDestination()
{
	//If we were on the way to ReachGoal - grab the object. Calculate position to return to. Teleport to this position before it can get stale...
	if (IsPerformingGesture)
	{
		ParentClassUseEquippedItem();
		MoveHandToDestination();
		IsPerformingGesture = false;
	}
	else
	{
		ManagedHandCollisionSphere->AttachToComponent(ItemAnchorComponent, FAttachmentTransformRules::SnapToTargetNotIncludingScale);
		IsReadyToMoveAgain = true;
	}
}
