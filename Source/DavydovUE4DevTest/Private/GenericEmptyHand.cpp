// Fill out your copyright notice in the Description page of Project Settings.


#include "GenericEmptyHand.h"
#include "Components/SphereComponent.h"

void AGenericEmptyHand::StopUsingThisItem()
{
}

void AGenericEmptyHand::ThisItemHasBeenEquipped(UHandItemManager* INManagerForThisItem)
{
	Super::ThisItemHasBeenEquipped(INManagerForThisItem);
	HandCollisionSphere = INManagerForThisItem->GetManagedCollisionSphere();
	ManagerForThisItem = INManagerForThisItem;
}

void AGenericEmptyHand::ItemUnequiped(bool DestroyOnUnequip, bool ShouldSimulatePhysicsOnUnequip)
{
	//To make sure that empty hand will be destroyed on unequip - we are passing true into Super::ItemUnequped. Doesn't matter if ShouldSimulatePhysicsOnUnequip - will be destroyed anyway
	Super::ItemUnequiped(true, ShouldSimulatePhysicsOnUnequip);
}

void AGenericEmptyHand::UseThisItem()
{
	TSet<AActor*> OverlappingActors;
	HandCollisionSphere->GetOverlappingActors(OverlappingActors, AEquipableItemBase::StaticClass());
	if (OverlappingActors.Num() == 0) 
	{	
		UE_LOG(LogTemp, Warning, TEXT("No actors in the graspable sphere"));
		if (GEngine)
			GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Orange, FString::Printf(TEXT("No actors in the graspable sphere")/*, *this->GetName()*/));
		return;	
	}
	AEquipableItemBase* L_ItemOfIterest = Cast<AEquipableItemBase>(*OverlappingActors.begin());
	if (!L_ItemOfIterest) { return; }
	ManagerForThisItem->EquipItem(L_ItemOfIterest);
}