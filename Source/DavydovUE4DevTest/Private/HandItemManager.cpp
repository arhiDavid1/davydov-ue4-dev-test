// Fill out your copyright notice in the Description page of Project Settings.


#include "HandItemManager.h"
#include "Components/SphereComponent.h"
#include "PlayerInventoryManagerInterface.h"

// Sets default values for this component's properties
UHandItemManager::UHandItemManager()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;
}


void UHandItemManager::SetHandCollisionSphere(USphereComponent* HandCollisionSphere)
{
	if (!HandCollisionSphere) return;
	ManagedHandCollisionSphere = HandCollisionSphere;
	SetItemAttachmentPoint(ManagedHandCollisionSphere->GetAttachParent());
}

void UHandItemManager::SetItemAttachmentPoint(USceneComponent* ComponentToAttachItemsTo)
{
	if (!ComponentToAttachItemsTo) return;
	ItemAnchorComponent = ComponentToAttachItemsTo;
}

void UHandItemManager::EquipItem(AEquipableItemBase* ItemToEquip)
{
	if (EquippedItem)
	{
		ReleaseHeldItem(false);
		//TODO: Put already equipped item in the storage or drop it or do something.
		//AddItemToStorage();
	}
	if (!ItemToEquip) return;
	ItemToEquip->AttachToComponent(ManagedHandCollisionSphere, FAttachmentTransformRules(EAttachmentRule::SnapToTarget, true));
	EquippedItem = ItemToEquip;
	EquippedItem->ThisItemHasBeenEquipped(this);
}

void UHandItemManager::UseEquippedItem()
{
	if (!EquippedItem) return;
	EquippedItem->UseThisItem();
}

void UHandItemManager::StopUsingItem()
{
	if (!EquippedItem) return;
	EquippedItem->StopUsingThisItem();
}

void UHandItemManager::ReleaseHeldItem(bool ShouldEquipEmptyHand)
{
	if (!EquippedItem) return;
	EquippedItem->ItemUnequiped(false);
	EquippedItem = nullptr;
	if (ShouldEquipEmptyHand)
	{
		EquipEmptyHand();
	}
}

void UHandItemManager::OnComponentDestroyed(bool bDestroyingHierarchy)
{
	if (EquippedItem)
	{
		EquippedItem->ItemUnequiped();
	}
}

// Called when the game starts
void UHandItemManager::BeginPlay()
{
	Super::BeginPlay();

	UWorld* L_World = GetWorld();
	if (!L_World) return;

	auto PlayerController = L_World->GetFirstPlayerController(); //TODO - won't work in multilayer but this is beyond this test task
	if (!PlayerController) return;

	IPlayerInventoryManagerInterface* PlayerInventoryManagerInterface = nullptr;

	for (UActorComponent* Component : PlayerController->GetComponents())
	{
		PlayerInventoryManagerInterface = Cast<IPlayerInventoryManagerInterface>(Component);
		if (PlayerInventoryManagerInterface)
		{
			break;
		}
	}

	if (PlayerInventoryManagerInterface == nullptr) return;

	TSubclassOf<AEquipableItemBase> ClassOfSavedItemToEquip;

	if (ComponentTags.Find(FName("R_HandItemManager")))
	{
		ClassOfSavedItemToEquip = PlayerInventoryManagerInterface->GetLeftHandItem();
	}
	else
	{
		ClassOfSavedItemToEquip = PlayerInventoryManagerInterface->GetRightHandItem();
	}

	if (!ClassOfSavedItemToEquip)
	{
		UE_LOG(LogTemp, Warning, TEXT("Failed to get ClassOfSavedItemToEquip - equipping empty hand"));
		EquipEmptyHand();
	}
	else
	{
		auto ItemToEquip = GetWorld()->SpawnActor<AEquipableItemBase>(ClassOfSavedItemToEquip);
		EquipItem(ItemToEquip);
	}
}

void UHandItemManager::EquipEmptyHand()
{
	if (!EquippedItem)
	{
		AEquipableItemBase* DefaultItem = GetWorld()->SpawnActor<AEquipableItemBase>(ItemEquipedByDefault);
		if (DefaultItem)
		{
			EquipItem(DefaultItem);
		}
	}
}

// Called every frame
void UHandItemManager::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
}

