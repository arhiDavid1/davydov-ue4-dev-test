// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "HandyWayGameMode.generated.h"

/**
 * 
 */
UCLASS()
class AHandyWayGameMode : public AGameModeBase
{
	GENERATED_BODY()

	AHandyWayGameMode();
	
public:
	/** Tries to spawn the player's pawn, at the location returned by FindPlayerStart */
	virtual void RestartPlayer(AController* NewPlayer) override;

	/** Transitions to calls BeginPlay on actors. */
	virtual void StartPlay() override;

	/** Called after a successful login.  This is the first place it is safe to call replicated functions on the PlayerController. */
	virtual void PostLogin(APlayerController* NewPlayer) override;

private:
	UFUNCTION()
	void HandlePlayerCharacterDestroyed(AActor* DestroyedActor);

	void BindOnDeathEvent(AController* NewPlayer);

	UFUNCTION(Exec)
	void LoadSecondLevel();

	UFUNCTION(Exec)
	void LoadFirstLevel();

	void SavePlayerInventoryForMapTravel();
};
