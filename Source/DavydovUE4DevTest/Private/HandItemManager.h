// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "EquipableItemBase.h"
#include "HandItemManager.generated.h"

class USphereComponent;

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class UHandItemManager : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UHandItemManager();

	void SetHandCollisionSphere(USphereComponent* HandCollisionSphere);

	void SetItemAttachmentPoint(USceneComponent* ComponentToAttachItemsTo);

	USphereComponent* GetManagedCollisionSphere() const { return ManagedHandCollisionSphere; };

	void EquipItem(AEquipableItemBase* ItemToEquip);

	virtual void UseEquippedItem();

	virtual void StopUsingItem();

	//Literally release player's grip on the item. What happens next is not up to this function. Also - equips empty hand.
	virtual void ReleaseHeldItem(bool ShouldEquipEmptyHand = true);

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<AEquipableItemBase> ItemEquipedByDefault;

	TSubclassOf<AEquipableItemBase> GetEquippedItemClass() { return EquippedItem->GetClass(); };

	UPROPERTY(VisibleAnywhere)
	USphereComponent* ManagedHandCollisionSphere;

	/**
	 * Called when a component is destroyed
	*
	* @param	bDestroyingHierarchy  - True if the entire component hierarchy is being torn down, allows avoiding expensive operations
	*/
	virtual void OnComponentDestroyed(bool bDestroyingHierarchy) override;

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

	void EquipEmptyHand();

	UPROPERTY(VisibleAnywhere)
	USceneComponent* ItemAnchorComponent;

	UPROPERTY(VisibleAnywhere)
	AEquipableItemBase* EquippedItem;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;
};
