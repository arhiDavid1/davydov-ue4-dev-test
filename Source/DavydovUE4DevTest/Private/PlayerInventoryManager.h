// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "PlayerInventoryManagerInterface.h"
#include "PlayerInventoryManager.generated.h"

class UHandItemManager;
class UHandyWaySaveGame;

/**
 *	Main function of player inventory manager is to save player's inventory between map transitions.
 */
UCLASS(Blueprintable, ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class UPlayerInventoryManager : public UActorComponent, public IPlayerInventoryManagerInterface
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UPlayerInventoryManager();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

	virtual void InitializeComponent() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	virtual void SavePlayerInventory() override;

	UFUNCTION()
	virtual TSubclassOf<AEquipableItemBase> GetLeftHandItem() override;

	UFUNCTION()
	virtual TSubclassOf<AEquipableItemBase> GetRightHandItem() override;

protected:

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	TSubclassOf<AEquipableItemBase> ItemInTheRightHand;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	TSubclassOf<AEquipableItemBase> ItemInTheLeftHand;

private:
	UPROPERTY()
	UHandyWaySaveGame* LoadedSaveGame;
};
