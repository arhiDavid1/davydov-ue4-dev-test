// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "GenericEmptyHand.h"
#include "PlayerInventoryManagerInterface.generated.h"

// This class does not need to be modified.
UINTERFACE(MinimalAPI)
class UPlayerInventoryManagerInterface : public UInterface
{
	GENERATED_BODY()
};

/**
 * 
 */
class IPlayerInventoryManagerInterface
{
	GENERATED_BODY()

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:
	virtual void SavePlayerInventory() = 0;

	//virtual void GetLeftHandItem(TSubclassOf<AGenericEmptyHand> &OUTHeldItemClass) = 0;
	virtual TSubclassOf<AEquipableItemBase> GetLeftHandItem() = 0;

	//virtual void GetRightHandItem(TSubclassOf<AGenericEmptyHand> &OUTHeldItemClass) = 0;
	virtual TSubclassOf<AEquipableItemBase> GetRightHandItem() = 0;
};
