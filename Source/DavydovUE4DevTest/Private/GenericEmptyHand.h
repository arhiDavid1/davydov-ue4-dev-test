// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "EquipableItemBase.h"
#include "HandItemManager.h"
#include "GenericEmptyHand.generated.h"

/**
 * 
 */
UCLASS()
class AGenericEmptyHand : public AEquipableItemBase
{
	GENERATED_BODY()
	
public:
	UFUNCTION()
	virtual void UseThisItem() override;

	UFUNCTION()
	virtual void StopUsingThisItem() override;

	UFUNCTION()
	virtual void ThisItemHasBeenEquipped(UHandItemManager* INManagerForThisItem) override;

	//Overriden parent unequip that destroys empty hand
	virtual void ItemUnequiped(bool DestroyOnUnequip = false, bool ShouldSimulatePhysicsOnUnequip = true) override;

private:
	UPROPERTY(VisibleAnywhere)
	UHandItemManager* ManagerForThisItem;

	UPROPERTY(VisibleAnywhere)
	USphereComponent* HandCollisionSphere;
};
