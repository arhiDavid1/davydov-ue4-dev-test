// Fill out your copyright notice in the Description page of Project Settings.


#include "BasicEnemy.h"
#include "HealthComponent.h"
#include "DrawDebugHelpers.h"

// Sets default values
ABasicEnemy::ABasicEnemy()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ABasicEnemy::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void ABasicEnemy::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

float ABasicEnemy::TakeDamage(float DamageAmount, FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser)
{
	float ActualDamage = DamageAmount;
	FVector TextLocation = GetActorLocation() + FVector::UpVector*120;
	HealthComponent->ChangeHealthAmount(-DamageAmount);
	DrawDebugString(GetWorld(), TextLocation, FString::SanitizeFloat(DamageAmount,0),0,FColor::Red,3,true,1);
	return ActualDamage;
}

void ABasicEnemy::PostInitializeComponents()
{
	Super::PostInitializeComponents();
	auto L_HealthComponent = Cast<UHealthComponent>(GetComponentByClass(UHealthComponent::StaticClass()));
	if (L_HealthComponent)
	{
		HealthComponent = L_HealthComponent;
		HealthComponent->OnCharacterDied.AddDynamic(this, &ABasicEnemy::HandleEnemyDied);
	}
}

