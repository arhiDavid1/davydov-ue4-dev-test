// Fill out your copyright notice in the Description page of Project Settings.


#include "PlayerInventoryManager.h"
#include "GenericEmptyHand.h"
#include "HandItemManager.h"
#include "Kismet/GameplayStatics.h"
#include "HandyWaySaveGame.h"


// Sets default values for this component's properties
UPlayerInventoryManager::UPlayerInventoryManager()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;
	bWantsInitializeComponent = true;
}


// Called when the game starts
void UPlayerInventoryManager::BeginPlay()
{
	Super::BeginPlay();
}

void UPlayerInventoryManager::InitializeComponent()
{
	Super::InitializeComponent();

	LoadedSaveGame = Cast<UHandyWaySaveGame>(UGameplayStatics::LoadGameFromSlot(FString("SlotOne"), 1));
	if (LoadedSaveGame)
	{
		ItemInTheRightHand = LoadedSaveGame->SavedItemInTheRightHand;
		ItemInTheLeftHand = LoadedSaveGame->SavedItemInTheLeftHand;
	}
}

// Called every frame
void UPlayerInventoryManager::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

void UPlayerInventoryManager::SavePlayerInventory()
{
	AController* L_OwningController = Cast<AController>(GetOwner());
	if (!L_OwningController) return;

	auto L_LeftHandItemManagerArray = L_OwningController->GetPawn()->GetComponentsByTag(UHandItemManager::StaticClass(), FName("L_HandItemManager"));
	if (L_LeftHandItemManagerArray.Num() == 1)
	{
		auto LeftHandItemManagerPrt = Cast<UHandItemManager>(L_LeftHandItemManagerArray[0]);
		if (LeftHandItemManagerPrt)
		{
			ItemInTheLeftHand = LeftHandItemManagerPrt->GetEquippedItemClass();
		}
	} 

	auto L_RightHandItemManagerArray = L_OwningController->GetPawn()->GetComponentsByTag(UHandItemManager::StaticClass(), FName("R_HandItemManager"));
	if (L_RightHandItemManagerArray.Num() == 1)
	{
		auto RightHandItemManagerPrt = Cast<UHandItemManager>(L_RightHandItemManagerArray[0]);
		if (RightHandItemManagerPrt)
		{
			ItemInTheRightHand = RightHandItemManagerPrt->GetEquippedItemClass();
		}
	}

	if (!LoadedSaveGame)
	{
		LoadedSaveGame = Cast<UHandyWaySaveGame>(UGameplayStatics::CreateSaveGameObject(UHandyWaySaveGame::StaticClass()));
	}
	if (LoadedSaveGame)
	{
		// Set data on the savegame object.
		LoadedSaveGame->SavedItemInTheLeftHand = ItemInTheLeftHand;
		LoadedSaveGame->SavedItemInTheRightHand = ItemInTheRightHand;

		// Save the data immediately.
		if (UGameplayStatics::SaveGameToSlot(LoadedSaveGame, FString("SlotOne"), 1))
		{
			UE_LOG(LogTemp, Warning, TEXT("Save succeeded"));
		}
	}
}

TSubclassOf<AEquipableItemBase> UPlayerInventoryManager::GetLeftHandItem()
{
	return ItemInTheLeftHand;
}

TSubclassOf<AEquipableItemBase> UPlayerInventoryManager::GetRightHandItem()
{
	return ItemInTheRightHand;
}



