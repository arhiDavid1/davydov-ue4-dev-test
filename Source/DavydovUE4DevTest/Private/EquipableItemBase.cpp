// Fill out your copyright notice in the Description page of Project Settings.


#include "EquipableItemBase.h"
#include "HandItemManager.h"

// Sets default values
AEquipableItemBase::AEquipableItemBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
}

void AEquipableItemBase::ItemUnequiped(bool DestroyOnUnequip, bool ShouldSimulatePhysicsOnUnequip)
{
	DetachFromActor(FDetachmentTransformRules::KeepWorldTransform);
	ThisItemManager = nullptr;
	if (DestroyOnUnequip)
	{
		Destroy();
		return;
	}
	auto PhysicalAsset = Cast<UPrimitiveComponent>(GetRootComponent());
	PhysicalAsset->SetSimulatePhysics(ShouldSimulatePhysicsOnUnequip);
}

// Called when the game starts or when spawned
void AEquipableItemBase::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void AEquipableItemBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}


void AEquipableItemBase::UseThisItem()
{
}

void AEquipableItemBase::StopUsingThisItem()
{

}

void AEquipableItemBase::ThisItemHasBeenEquipped(UHandItemManager* ManagerForThisItem)
{
	ThisItemManager = ManagerForThisItem;
}

