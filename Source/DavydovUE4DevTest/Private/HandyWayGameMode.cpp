// Fill out your copyright notice in the Description page of Project Settings.


#include "HandyWayGameMode.h"
#include "PlayerInventoryManagerInterface.h"

AHandyWayGameMode::AHandyWayGameMode()
{
	//bUseSeamlessTravel = true;
}

void AHandyWayGameMode::RestartPlayer(AController* NewPlayer)
{
	Super::RestartPlayer(NewPlayer);
	BindOnDeathEvent(NewPlayer);
}

void AHandyWayGameMode::StartPlay()
{
	Super::StartPlay();

}

void AHandyWayGameMode::PostLogin(APlayerController* NewPlayer)
{
	Super::PostLogin(NewPlayer);

}

void AHandyWayGameMode::HandlePlayerCharacterDestroyed(AActor* DestroyedActor)
{
	RestartPlayer(GetWorld()->GetFirstPlayerController());
}

void AHandyWayGameMode::BindOnDeathEvent(AController* NewPlayer)
{
	if (!NewPlayer) return;
	auto APlayerPawn = NewPlayer->GetPawn();
	if (!APlayerPawn) return;
	APlayerPawn->OnDestroyed.AddDynamic(this, &AHandyWayGameMode::HandlePlayerCharacterDestroyed);
	if (NewPlayer->GetPawn()->OnDestroyed.IsBound())
	{
		UE_LOG(LogTemp, Warning, TEXT("Successfully bound on player pawn destruct"));
		if (GEngine)
			GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Orange, FString::Printf(TEXT("Successfully bound on player pawn destruct")/*, *this->GetName()*/));
	};
}

void AHandyWayGameMode::LoadSecondLevel()
{
	UE_LOG(LogTemp, Warning, TEXT("Load second level called"));

	UWorld* L_World = GetWorld();
	if (!L_World) return;

	SavePlayerInventoryForMapTravel();

	if (L_World)
	{
		L_World->ServerTravel("/Game/ThirdPersonBP/Maps/ThirdPersonExampleMap");
	}
}


void AHandyWayGameMode::LoadFirstLevel()
{
	UE_LOG(LogTemp, Warning, TEXT("Load first level called"));

	UWorld* L_World = GetWorld();
	if (!L_World) return;

	SavePlayerInventoryForMapTravel();

	if (L_World)
	{
		L_World->ServerTravel("/Game/FirstPersonCPP/Maps/FirstPersonExampleMap");
	}
}

void AHandyWayGameMode::SavePlayerInventoryForMapTravel()
{
	UWorld* L_World = GetWorld();
	if (!L_World) return;

	auto PlayerController = L_World->GetFirstPlayerController(); //TODO - won't work in multilayer but this is beyond this test task
	if (!PlayerController) return;

	IPlayerInventoryManagerInterface* PlayerInventoryManagerInterface = nullptr;

	for (UActorComponent* Component : PlayerController->GetComponents())
	{
		PlayerInventoryManagerInterface = Cast<IPlayerInventoryManagerInterface>(Component);
		if (PlayerInventoryManagerInterface)
		{
			break;
		}
	}

	if (PlayerInventoryManagerInterface == nullptr) return;

	PlayerInventoryManagerInterface->SavePlayerInventory();
}
