// Fill out your copyright notice in the Description page of Project Settings.


#include "Pistol.h"
#include "../DavydovUE4DevTestProjectile.h"
#include "HandItemManager.h"

APistol::APistol()
{
	// Create a gun mesh component
	FP_Gun = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("FP_Gun"));
	FP_Gun->SetOnlyOwnerSee(false);		
	SetRootComponent(FP_Gun);

	FP_MuzzleLocation = CreateDefaultSubobject<USceneComponent>(TEXT("MuzzleLocation"));
	FP_MuzzleLocation->SetupAttachment(FP_Gun);

	// Default offset from the character location for projectiles to spawn
	GunOffset = FVector(100.0f, 0.0f, 10.0f);
}

void APistol::ItemUnequiped(bool DestroyOnUnequip, bool ShouldSimulatePhysicsOnUnequip)
{
	Super::ItemUnequiped(DestroyOnUnequip, ShouldSimulatePhysicsOnUnequip);
}

void APistol::UseThisItem()
{
	FirePistol();
}

void APistol::ThisItemHasBeenEquipped(UHandItemManager* ManagerForThisItem)
{
	Super::ThisItemHasBeenEquipped(ManagerForThisItem);
	SetActorRelativeRotation(FQuat(FRotator(0.0f, -90.0f, 0.0f)));
}

void APistol::FirePistol()
{
	// try and fire a projectile
	if (ProjectileClass != nullptr)
	{
		UWorld* const World = GetWorld();
		if (World != nullptr)
		{
			const FRotator SpawnRotation = FP_MuzzleLocation->GetComponentRotation();
			// MuzzleOffset is in camera space, so transform it to world space before offsetting from the character location to find the final muzzle position
			const FVector SpawnLocation = ((FP_MuzzleLocation != nullptr) ? FP_MuzzleLocation->GetComponentLocation() : GetActorLocation()) + SpawnRotation.RotateVector(GunOffset);

			//Set Spawn Collision Handling Override
			FActorSpawnParameters ActorSpawnParams;
			ActorSpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButDontSpawnIfColliding;
			APawn* ShotInstigator = Cast<APawn>(ThisItemManager->GetOwner());
			if (ShotInstigator)
			{
				ActorSpawnParams.Instigator = ShotInstigator;
			}

			// spawn the projectile at the muzzle
			World->SpawnActor<ADavydovUE4DevTestProjectile>(ProjectileClass, SpawnLocation, SpawnRotation, ActorSpawnParams);
		}
	}
}
