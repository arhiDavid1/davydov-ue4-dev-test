// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "HandyWayCharacter.generated.h"

class UCameraComponent;
class UMotionControllerComponent;
class USphereComponent;
class UHandItemManager;
class UHealthComponent;
class UWidgetInteractionComponent;

UCLASS()
class AHandyWayCharacter : public ACharacter
{
	GENERATED_BODY()

	/** First person camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	UCameraComponent* FirstPersonCameraComponent;

	/** Motion controller (right hand) */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	UMotionControllerComponent* R_MotionController;

	/** Motion controller (left hand) */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	UMotionControllerComponent* L_MotionController;

	//TODO: It might be reasonable to move collision sphere into EmptyHand class since we care for collisions only when we have nothing in hands
	/** Collision sphere that detects objects we cant interact with (Right hand) */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	USphereComponent* R_HandCollisionSphere;

	/** Collision sphere that detects objects we cant interact with (Left hand) */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	USphereComponent* L_HandCollisionSphere;

	/** Component responsible for manipulating (picking up, dropping, storing) items held in the hand (Right hand) */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	UHandItemManager* R_HandItemManager;

	/** Component responsible for manipulating (picking up, dropping, storing) items held in the hand (Left hand) */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	UHandItemManager* L_HandItemManager;

	/** Iteractin with inworld widgets (Right hand) */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	UWidgetInteractionComponent* R_WidgetInteractor;

	/** Iteractin with inworld widgets (Left hand) */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	UWidgetInteractionComponent* L_WidgetInteractor;

	/** Component responsible for tracking character's health */
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"))
	UHealthComponent* HealthComponent;

	/** Pause menu widget */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"))
	TSubclassOf<AActor> MenuWidget;

public:
	// Sets default values for this character's properties
	AHandyWayCharacter();

	bool bUsingMotionControllers : 1;

protected:
	/** Resets HMD orientation and position in VR. */
	void OnResetVR();

	/** Handles moving forward/backward */
	void MoveForward(float Val);

	/** Handles strafing movement, left and right */
	void MoveRight(float Val);

	void UseItemInRightHand();
	void StopUsingItemInRightHand();
	void DropItemInRightHand();

	void UseItemInLeftHand();
	void StopUsingItemInLeftHand();
	void DropItemInLeftHand();
	
	void ToggleMenu();

	UFUNCTION()
	void StartDeathSequence();

	AActor* MenuActor;

public:	//Parent classes overrides

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	// Called every frame
	virtual void Tick(float DeltaTime) override;

	virtual float TakeDamage(float DamageAmount, struct FDamageEvent const& DamageEvent, class AController* EventInstigator, AActor* DamageCauser) override;
	
	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	virtual void PostInitializeComponents() override;
};
