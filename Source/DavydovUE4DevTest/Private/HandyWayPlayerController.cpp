// Fill out your copyright notice in the Description page of Project Settings.


#include "HandyWayPlayerController.h"
#include "Kismet/GameplayStatics.h"

void AHandyWayPlayerController::ClearSaveFile()
{
	UGameplayStatics::DeleteGameInSlot(FString("SlotOne"), 1);
}