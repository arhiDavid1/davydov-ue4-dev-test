// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "HandItemManager.h"
#include "NoVRHandItemManager.generated.h"

/**
 * NoVRHandItemManager is intended to be used without VR
 * depending on held item properties it performs certain actions before using the item
 * simulating appropriate hand movements. For example:
 * - if held item is empty hand - first it moves forward until it reaches maximum distance or finds an item that can be grabbed
 * - if item is grenade - hand is quickly moves forward, simulating throw, and then releases the grenade
 * (description above indicates how class should be used - not how it works at the moment - some or all features might be missing)
 */

class UTimelineComponent;

UCLASS()
class UNoVRHandItemManager : public UHandItemManager
{
	GENERATED_BODY()

	UNoVRHandItemManager();

	UPROPERTY()
	float ReachDistance = 500;

	UPROPERTY()
	float TimeToReachGoal = 1;
	
public:
	virtual void BeginPlay() override;

	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	virtual void UseEquippedItem() override;

	virtual void StopUsingItem() override;

	UPROPERTY(EditAnywhere)
	UCurveFloat* MovementCurve;

private:
	void ParentClassUseEquippedItem();

	void ParentClassStopUsingItem();

	void MoveHandToDestination();

	//Location where grab will be performed
	FVector GetReachDestination();

	//Point at which player is looking at
	FVector GetPointOfInterest();

	FVector ReachGoal;

	/////////////////////////////////
	/// Hand movement timeline setup
	UFUNCTION()
	void ControlHandMovement();

	UFUNCTION()
	void HandReachedDestination();

	bool IsPerformingGesture;
	bool ReachedDestination;
	bool IsReadyToMoveAgain;
	float DistanceTraveledValue;
	float CurveFloatValue;
	float TimelineValue;
	UTimelineComponent* MoveTimeline;
};
