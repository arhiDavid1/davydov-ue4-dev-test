// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once 

#include "CoreMinimal.h"
#include "GameFramework/HUD.h"
#include "DavydovUE4DevTestHUD.generated.h"

UCLASS()
class ADavydovUE4DevTestHUD : public AHUD
{
	GENERATED_BODY()

public:
	ADavydovUE4DevTestHUD();

	/** Primary draw call for the HUD */
	virtual void DrawHUD() override;

private:
	/** Crosshair asset pointer */
	class UTexture2D* CrosshairTex;

};

