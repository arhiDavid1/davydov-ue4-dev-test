// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class DavydovUE4DevTest : ModuleRules
{
	public DavydovUE4DevTest(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

		PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore", "HeadMountedDisplay", "UMG" });
	}
}
