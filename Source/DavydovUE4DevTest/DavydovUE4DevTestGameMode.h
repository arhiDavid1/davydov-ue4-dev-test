// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "DavydovUE4DevTestGameMode.generated.h"

UCLASS(minimalapi)
class ADavydovUE4DevTestGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ADavydovUE4DevTestGameMode();
};



